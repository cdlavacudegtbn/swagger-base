# Plantilla base para documentar API con Swagger

# Pre-commit

Antes de enviar un commit la app verifica que el proyecto tenga el formato configurado (Prettier-> `npm run format:check`):

- Si no se envía el commit ejecutar:
  - `npm run format:write`
  - `git add ....`
  - `git commit -m ....`

# Correr Docker

Para ejecutar la app(puerto: API_PORT) en Docker correr:

- `docker compose up -d`
- `docker compose logs app -f`: Logs de desarrollo de la app (el contenedor corre nodemon internamente)
- `docker compose exec app bash`: Ejecutar consola de bash para entrar al contenedor y correr comandos desde aquí

# Documentación con swagger

- [Swagger OpenApi Guide](https://swagger.io/docs/specification/about/)
