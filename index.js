const express = require('express')
const app = express()
const cors = require('cors')
const swaggerUI = require('swagger-ui-express')
const docs = require('./docs')

require('dotenv').config()
const port = process.env.API_PORT || 3000

app.use(cors())
app.options('*', cors())
app.use(express.json({ limit: '150mb' }))
app.use(express.urlencoded({ limit: '150mb', extended: true }))

app.use('/docs', swaggerUI.serve, swaggerUI.setup(docs))

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
